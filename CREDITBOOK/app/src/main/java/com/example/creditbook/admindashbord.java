package com.example.creditbook;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.FirebaseDatabase;

public class admindashbord extends AppCompatActivity
{
    RecyclerView recview;
    myadapter adapter;
    FloatingActionButton msignin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admindashbord);
        setTitle("Enter CustomerId...");
        msignin=findViewById(R.id.signin);

        msignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(admindashbord.this, "Register Your Customer", Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(admindashbord.this,customersign.class);
                startActivity(intent);
            }
        });

        recview=(RecyclerView)findViewById(R.id.recview);
        recview.setLayoutManager(new LinearLayoutManager(this));

        FirebaseRecyclerOptions<CustomerHelperClass> options = new FirebaseRecyclerOptions.Builder<CustomerHelperClass>()
                .setQuery(FirebaseDatabase.getInstance().getReference().child("Customer"), CustomerHelperClass.class)
                .build();

        adapter=new myadapter(options);
        recview.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.searchmenu,menu);

        MenuItem item=menu.findItem(R.id.search);

        SearchView searchView=(SearchView)item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String s) {

                processsearch(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                processsearch(s);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void processsearch(String s)
    {
        FirebaseRecyclerOptions<CustomerHelperClass> options = new FirebaseRecyclerOptions.Builder<CustomerHelperClass>()
                .setQuery(FirebaseDatabase.getInstance().getReference().child("Customer").orderByChild("customerid").startAt(s).endAt(s+"\uf8ff"), CustomerHelperClass.class)
                .build();

        adapter=new myadapter(options);
        adapter.startListening();
        recview.setAdapter(adapter);

    }
}


























