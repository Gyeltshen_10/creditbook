package com.example.creditbook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class customersign extends AppCompatActivity {
    private static final String[] DEPARTMENTS =new String[]{
            "BScCS",  "BScIT", "Staff"
    };
    private TextView mname, memail,mid,mpassword;
    AutoCompleteTextView mdepartment;
    Button msignup;

    private ProgressDialog mLoadingBar;
    private FirebaseAuth mAuth;
    FirebaseDatabase rootNode;
    DatabaseReference reference;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customersign);
        getSupportActionBar().hide();

        mname=findViewById(R.id.cname);
        memail=findViewById(R.id.cemail);
        mid=findViewById(R.id.staffidstudentid);
        mpassword=findViewById(R.id.cpassword);
        mdepartment=findViewById(R.id.selectdep);
        msignup=findViewById(R.id.signinbutton2);

        AutoCompleteTextView editText = findViewById(R.id.selectdep);
        ArrayAdapter <String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, DEPARTMENTS);
        editText.setAdapter(adapter);

        mAuth = FirebaseAuth.getInstance();
        mLoadingBar = new ProgressDialog(customersign.this);

        msignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCrededentials();
                rootNode = FirebaseDatabase.getInstance();
                reference = rootNode.getReference("Customer");

                String email = memail.getText().toString();
                String name = mname.getText().toString();
                String password = mpassword.getText().toString();
                String department = mdepartment.getText().toString();
                String customerid = mid.getText().toString();

               CustomerHelperClass helperClass = new CustomerHelperClass(name, email, password, department, customerid);
                reference.child(customerid).setValue(helperClass);

            }
        });
    }

    private void checkCrededentials() {
        String email = memail.getText().toString();
        String name = mname.getText().toString();
        String password = mpassword.getText().toString();
        String department= mdepartment.getText().toString();
        String customerid = mid.getText().toString();

        if (department.isEmpty()) {
            mdepartment.setError("invalid number");
            mdepartment.requestFocus();
        }

        else if (name.isEmpty() || name.length() < 3) {
            mname.setError( "Username is not valid");
            mname.requestFocus();
        }

        else if (email.isEmpty() || !email.contains("@")) {
            memail.setError( "email.is not valid");
            memail.requestFocus();
        }


        else if (password.isEmpty() || password.length() < 6) {
            mpassword.setError("Password is not valid");
            mpassword.requestFocus();
        }

        else if (customerid.isEmpty()) {
            mid.setError("invalid CustomerId");
            mid.requestFocus();
        }
        else{
            mLoadingBar.setTitle("Signing Up");
            mLoadingBar.setMessage("Please wait while Signing Up");
            mLoadingBar.setCanceledOnTouchOutside(false);
            mLoadingBar.show();
            mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        mAuth.getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    Toast.makeText(customersign.this, "Successful registration,Check your email to verify", Toast.LENGTH_SHORT).show();
                                    mLoadingBar.dismiss();
                                    Intent intent= new Intent(com.example.creditbook.customersign.this,admindashbord.class);
                                    intent.putExtra("customerid",customerid);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                                else{
                                    Toast.makeText(customersign.this,task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }

                    else{
                        Toast.makeText(customersign.this,task.getException().toString(), Toast.LENGTH_SHORT).show();
                    }

                }
            });

        }

    }

}
